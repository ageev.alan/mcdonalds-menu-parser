/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
require('dotenv').config();
const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({ defaultViewport: { width: 600, height: 800 } });
  const page = await browser.newPage();
  await page.goto(process.env.PAGE_URL);

  const productsData = [];

  page.on('response', async (response) => {
    try {
      if (response.url().includes(process.env.API_URL)) {
        const productData = await response.json();
        console.log(`GET ${productData.offers[0].name}`);
        productsData.push(productData);
      }
    } catch (error) { /** */ }
  });

  const categoriesSelector = '.menu-categories__wrap > .menu-categories__item';
  const productSelector = '.catalog-product';
  const closeButtonSelector = '.base-close-button';

  const categories = await page.$$(categoriesSelector);

  const getProducts = async () => {
    await page.waitForTimeout(1000);
    await page.waitForSelector(productSelector);
    const products = await page.$$(productSelector);

    for (const product of products) {
      await product.click();
      await page.waitForSelector(closeButtonSelector);
      const closeButton = await page.$(closeButtonSelector);
      await page.waitForTimeout(1000);
      await closeButton.click();
      await page.evaluate(() => window.scrollTo(0, 0));
      await page.waitForTimeout(1000);
    }
  };

  const navigateThroughCategories = async () => {
    for (const category of categories) {
      const categoryText = await page.evaluate(async (el) => el.textContent, category);
      console.log(categoryText);

      await category.click();
      await getProducts();
    }
    console.log(productsData.length);
  };

  console.time('Mcdonalds job');
  await navigateThroughCategories();
  console.timeEnd('Mcdonalds job');
  await browser.close();
})();
